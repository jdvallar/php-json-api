<?php
/**
 *  Copyright 2017-2018 Justin Dane D. Vallar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

namespace Vallarj\JsonApi;


use JsonSerializable;
use Vallarj\JsonApi\Encoder\ResourceExtractor;
use Vallarj\JsonApi\Exception\Exception;
use Vallarj\JsonApi\Exception\InvalidArgumentException;

class Encoder implements EncoderInterface
{
    /** @var SchemaManager */
    private $schemaManager;

    /** @var ResourceExtractor */
    private $resourceExtractor;

    /**
     * Encoder constructor.
     *
     * @param SchemaManager $schemaManager
     * @param ResourceExtractor $resourceExtractor
     */
    public function __construct(
        SchemaManager $schemaManager,
        ResourceExtractor $resourceExtractor
    ) {
        $this->schemaManager = $schemaManager;
        $this->resourceExtractor = $resourceExtractor;
    }

    /**
     * Creates an Encoder instance
     *
     * @param SchemaManager $schemaManager
     * @return Encoder
     */
    public static function create(SchemaManager $schemaManager): Encoder
    {
        return new Encoder(
            $schemaManager,
            new ResourceExtractor($schemaManager)
        );
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function encode(
        $resource,
        array $schemas,
        array $includedKeys = [],
        ?callable $showProperty = null,
        array $meta = []
    ): JsonSerializable {
        if (is_null($showProperty)) {
            $showProperty = function(string $resourceType, string $property): bool { return true; };
        }

        if (is_object($resource)) {
            $document = $this->resourceExtractor->extractSingleResource(
                $resource,
                $schemas,
                $includedKeys,
                $showProperty
            );
        } else if (is_array($resource)) {
            $document = $this->resourceExtractor->extractResourceCollection(
                $resource,
                $schemas,
                $includedKeys,
                $showProperty
            );
        } else {
            throw new InvalidArgumentException('Resource must be an object or an array of objects.');
        }

        // Set document meta
        $document->setMeta($meta);

        // Encode the data
        return $document;
    }
}