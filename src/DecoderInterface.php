<?php
/**
 *  Copyright 2017-2018 Justin Dane D. Vallar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

namespace Vallarj\JsonApi;


use Vallarj\JsonApi\Decoder\ResultInterface;
use Vallarj\JsonApi\Exception\InvalidFormatException;

interface DecoderInterface
{
    /**
     * Decodes a POST document into a new object from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @param bool $allowEphemeralId
     * @return ResultInterface
     * @throws InvalidFormatException
     */
    public function decodePostResource(string $data, array $schemas, bool $allowEphemeralId = false): ResultInterface;

    /**
     * Decodes a PATCH document into a new object from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @param mixed $expectedId     If not null, decoder will check if provided ID matches the expected ID
     * @return ResultInterface
     * @throws InvalidFormatException
     */
    public function decodePatchResource(string $data, array $schemas, $expectedId = null): ResultInterface;

    /**
     * Decodes a To-one relationship request into a new object from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @return mixed
     * @throws InvalidFormatException
     */
    public function decodeToOneRelationshipRequest(string $data, array $schemas);

    /**
     * Decodes a To-many relationship request into new objects from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @return mixed
     * @throws InvalidFormatException
     */
    public function decodeToManyRelationshipRequest(string $data, array $schemas);
}