<?php


namespace Vallarj\JsonApi\Decoder;


use stdClass;
use Vallarj\JsonApi\Decoder\ObjectResolver\Context;
use Vallarj\JsonApi\Exception\Exception;
use Vallarj\JsonApi\Exception\InvalidFormatException;
use Vallarj\JsonApi\Exception\SchemaNotRegisteredException;
use Vallarj\JsonApi\Schema\Accessible;
use Vallarj\JsonApi\Schema\Requirable;
use Vallarj\JsonApi\Schema\ResourceSchemaInterface;
use Vallarj\JsonApi\Schema\ToManyRelationshipInterface;
use Vallarj\JsonApi\Schema\ToOneRelationshipInterface;
use Vallarj\JsonApi\SchemaManager;

class ObjectResolver
{
    private const OPERATION_POST = 'POST';
    private const OPERATION_PATCH = 'PATCH';

    /** @var SchemaManager */
    private $schemaManager;

    /**
     * ObjectResolver constructor.
     *
     * @param SchemaManager $schemaManager
     */
    public function __construct(SchemaManager $schemaManager)
    {
        $this->schemaManager = $schemaManager;
    }

    /**
     * Resolves an object from a resource document
     *
     * @param stdClass $data
     * @param ResourceSchemaInterface $schema
     * @param string $operation
     * @return ResultInterface
     * @throws Exception
     * @throws InvalidFormatException
     */
    public function resolve(stdClass $data, ResourceSchemaInterface $schema, string $operation): ResultInterface
    {
        // Create context
        $context = $this->initializeContext($data, $schema, $operation);

        $attributeErrors = $this->validateAttributes($context);
        $relationshipErrors = $this->validateRelationships($context);

        if (empty($attributeErrors) && empty($relationshipErrors)) {
            // No errors
            $object = $this->createObject($context);
            $modifiedProperties = array_merge(
                array_keys($context->getAttributes()),
                array_keys($context->getToOneRelationships()),
                array_keys($context->getToManyRelationships())
            );

            return new Result($object, $modifiedProperties, [], []);
        } else {
            return new Result(null, [], $attributeErrors, $relationshipErrors);
        }
    }

    /**
     * Initializes context of the current create operation
     *
     * @param stdClass $data
     * @param ResourceSchemaInterface $schema
     * @param string $operation
     * @return Context
     * @throws Exception
     * @throws InvalidFormatException
     */
    public function initializeContext(
        stdClass $data,
        ResourceSchemaInterface $schema,
        string $operation
    ): Context {
        $context = new Context($operation, $schema);
        $context->setIdentifier($data->id ?? null);

        // Schema attributes
        $schemaAttributes = $schema->getAttributes();

        // Schema relationships
        $schemaRelationships = $schema->getRelationships();

        // Set attributes and relationships to context to prepare for validation
        // This is needed for interdependent validation
        // Set attributes
        if (property_exists($data, 'attributes')) {
            $attributes = $data->attributes;

            // Perform attribute pre-processing then add to current context array
            foreach($schemaAttributes as $schemaAttribute) {
                $key = $schemaAttribute->getKey();
                if (property_exists($attributes, $key)
                    && $this->isPropertyWritable($schemaAttribute, $operation)
                ) {
                    // Perform attribute pre-processing
                    $value = $schemaAttribute->filterValue($attributes->{$key});

                    // Add to context
                    $context->addAttribute($key, $value);
                }
            }
        }

        // Set relationships
        if (property_exists($data, 'relationships')) {
            $relationships = $data->relationships;

            // Add relationships to current context array
            foreach($schemaRelationships as $schemaRelationship) {
                $key = $schemaRelationship->getKey();
                if (!property_exists($relationships, $key)
                    || !$this->isPropertyWritable($schemaRelationship, $operation)) {
                    continue;
                }

                $data = $relationships->{$key}->data;
                if ($schemaRelationship instanceof ToOneRelationshipInterface) {
                    // Allow only nulls and objects
                    if(!is_null($data) && !is_object($data)) {
                        throw new InvalidFormatException("Invalid to-one relationship format. [$key]");
                    }

                    $context->addToOneRelationship($key, $data);
                } else if ($schemaRelationship instanceof ToManyRelationshipInterface) {
                    // Allow only arrays
                    if(!is_array($data)) {
                        throw new InvalidFormatException("Invalid to-many relationship format. [$key]");
                    }

                    $context->addToManyRelationship($key, $data);
                }
            }
        }

        return $context;
    }

    /**
     * Validates context data attributes. Returns an array of errors.
     *
     * @param Context $context
     * @return array
     */
    public function validateAttributes(Context $context): array
    {
        $schema = $context->getResourceSchema();
        $operation = $context->getOperation();
        $errors = [];

        $schemaAttributes = $schema->getAttributes();
        foreach($schemaAttributes as $schemaAttribute) {
            if (!$this->isPropertyWritable($schemaAttribute, $operation)) {
                // This will silently skip read-only attributes that have been changed in request
                continue;
            }

            $key = $schemaAttribute->getKey();
            $value = $context->getAttribute($key);

            if (is_null($value)) {
                // If PATCH operation and actually changing the attribute to null
                if ($operation === self::OPERATION_PATCH
                    && $context->hasAttribute($key)
                    && $schemaAttribute->isRequired()
                ) {
                    $errors[$key][] = "Field is required";
                    continue;
                }

                // If attribute is required
                if ($this->isPropertyRequired($schemaAttribute, $operation)) {
                    $errors[$key][] = "Field is required";
                    continue;
                }

                // If validateIfEmpty is false, we should not attempt to validate
                if(!$schemaAttribute->validateIfEmpty()) {
                    continue;
                }
            }

            // Validate attribute
            $validationResult = $schemaAttribute->isValid($value, $context);
            if (!$validationResult->isValid()) {
                $errorMessages = $validationResult->getMessages();
                foreach ($errorMessages as $errorMessage) {
                    $errors[$key][] = $errorMessage;
                }
            }
        }

        return $errors;
    }

    /**
     * Validates context data relationships. Returns an array of errors.
     *
     * @param Context $context
     * @return array
     */
    public function validateRelationships(Context $context): array
    {
        $schema = $context->getResourceSchema();
        $operation = $context->getOperation();
        $errors = [];

        // Relationships
        $schemaRelationships = $schema->getRelationships();

        /** @var ToOneRelationshipInterface|ToManyRelationshipInterface $schemaRelationship */
        foreach($schemaRelationships as $schemaRelationship) {
            if (!$this->isPropertyWritable($schemaRelationship, $operation)) {
                // This will silently skip read-only relationships that have been changed in request
                continue;
            }

            $key = $schemaRelationship->getKey();

            if($schemaRelationship instanceof ToOneRelationshipInterface) {
                $relationship = $context->getToOneRelationship($key);
                $expectedSchemas = $schemaRelationship->getExpectedSchemas();

                if (is_null($relationship)) {
                    // If PATCH operation and actually changing the relationship to null
                    if ($operation === self::OPERATION_PATCH
                        && $context->hasToOneRelationship($key)
                        && $schemaRelationship->isRequired()
                    ) {
                        $errors[$key][] = "Field is required";
                        continue;
                    }

                    // If relationship is required
                    if ($this->isPropertyRequired($schemaRelationship, $operation)) {
                        $errors[$key][] = "Field is required";
                        continue;
                    }

                    // If validateIfEmpty is false, we should not attempt to validate
                    if (!$schemaRelationship->validateIfEmpty()) {
                        continue;
                    }
                }

                // Validate relationship
                if (!$this->hasCompatibleSchema($relationship->type, $expectedSchemas)) {
                    $errors[$key][] = "Cannot resolve resource type '$relationship->type'";
                } else {
                    // Custom validation
                    $validationResult = $schemaRelationship->isValid(
                        $relationship->id ?? null,
                        $relationship->type,
                        $context)
                    ;
                    if (!$validationResult->isValid()) {
                        $errorMessages = $validationResult->getMessages();
                        foreach($errorMessages as $errorMessage) {
                            $errors[$key][] = $errorMessage;
                        }
                    }
                }
            } else if($schemaRelationship instanceof ToManyRelationshipInterface) {
                $relationship = $context->getToManyRelationship($key);
                $expectedSchemas = $schemaRelationship->getExpectedSchemas();

                if (empty($relationship)) {
                    // If PATCH operation and actually emptying the relationship
                    if ($operation === self::OPERATION_PATCH
                        && $context->hasToManyRelationship($key)
                        && $schemaRelationship->isRequired()
                    ) {
                        $errors[$key][] = "Field is required";
                        continue;
                    }

                    // If relationship is required
                    if ($this->isPropertyRequired($schemaRelationship, $operation)) {
                        $errors[$key][] = "Field is required";
                        continue;
                    }

                    // If validateIfEmpty is false, we should not attempt to validate
                    if (!$schemaRelationship->validateIfEmpty()) {
                        continue;
                    }
                }

                // Validate relationships
                foreach ($relationship as $item) {
                    if (!$this->hasCompatibleSchema($item->type, $expectedSchemas)) {
                        $errors[$key][] = "Cannot resolve resource type '$item->type'";
                    }
                }

                // Custom validation
                if (!isset($errors[$key])) {
                    $validationResult = $schemaRelationship->isValid($relationship, $context);
                    if (!$validationResult->isValid()) {
                        $errorMessages = $validationResult->getMessages();
                        foreach ($errorMessages as $errorMessage) {
                            $errors[$key][] = $errorMessage;
                        }
                    }
                }
            }
        }

        return $errors;
    }

    /**
     * Creates a mapped object instance from a given context
     *
     * @param Context $context
     * @return mixed
     */
    public function createObject(Context $context)
    {
        $schema = $context->getResourceSchema();

        // Get the resource class
        $resourceClass = $schema->getMappingClass();

        // Create the object
        $object = new $resourceClass;

        // Set the resource ID
        if(!is_null($resourceId = $context->getIdentifier())) {
            $schema->getIdentifier()->setResourceId($object, $resourceId);
        }

        // Map attributes
        foreach ($context->getAttributes() as $key => $value) {
            $schemaAttribute = $schema->getAttribute($key);
            $schemaAttribute->setValue($object, $value);
        }

        // Map to-one relationships
        foreach ($context->getToOneRelationships() as $key => $resourceIdentifier) {
            /** @var ToOneRelationshipInterface $schemaRelationship */
            $schemaRelationship = $schema->getRelationship($key);

            if (is_null($resourceIdentifier)) {
                // Clear object
                $schemaRelationship->clearObject($object);
                continue;
            }

            // Retrieve compatible schema
            $compatibleSchema = $this->getCompatibleSchema(
                $resourceIdentifier->type,
                $schemaRelationship->getExpectedSchemas()
            );

            $relationshipObject = $this->resolveRelationshipObject(
                $context,
                $compatibleSchema,
                $resourceIdentifier->id
            );

            $schemaRelationship->setObject($object, $relationshipObject);
        }

        // Map to-many relationships
        foreach ($context->getToManyRelationships() as $key => $resourceCollection) {
            /** @var ToManyRelationshipInterface $schemaRelationship */
            $schemaRelationship = $schema->getRelationship($key);

            if (empty($resourceCollection)) {
                // Clear collection
                $schemaRelationship->clearCollection($object);
                continue;
            }

            foreach ($resourceCollection as $resource) {
                // Retrieve compatible schema
                $compatibleSchema = $this->getCompatibleSchema(
                    $resource->type,
                    $schemaRelationship->getExpectedSchemas()
                );

                $relationshipObject = $this->resolveRelationshipObject(
                    $context,
                    $compatibleSchema,
                    $resource->id
                );

                $schemaRelationship->addItem($object, $relationshipObject);
            }
        }

        return $object;
    }

    /**
     * Returns true if a property is writable in a given operation
     *
     * @param Accessible $property
     * @param string $operation
     * @return bool
     */
    private function isPropertyWritable(Accessible $property, string $operation): bool
    {
        return (($operation == self::OPERATION_POST && $property->isCreateWritable()) ||
            ($operation == self::OPERATION_PATCH && $property->isUpdateWritable()));
    }

    /**
     * Returns true if a property is required in a given operation
     *
     * @param Requirable $property
     * @param string $operation
     * @return bool
     */
    private function isPropertyRequired(Requirable $property, string $operation): bool
    {
        return (($operation == self::OPERATION_POST && $property->isRequired()) ||
            ($operation == self::OPERATION_PATCH && $property->isRequiredOnUpdate()));
    }

    /**
     * Returns true if a compatible schema exists for a given resource type
     *
     * @param string $resourceType
     * @param ResourceSchemaInterface[] $expectedSchemas
     * @return bool
     * @throws SchemaNotRegisteredException
     */
    private function hasCompatibleSchema(string $resourceType, array $expectedSchemas): bool
    {
        foreach ($expectedSchemas as $schemaName) {
            $schema = $this->schemaManager->get($schemaName);
            if ($schema->getResourceType() == $resourceType) {
                return true;
            }
        }

        return false;
    }

    /**
     * Returns a compatible schema for a given resource type
     *
     * @param string $resourceType
     * @param ResourceSchemaInterface[] $expectedSchemas
     * @return ResourceSchemaInterface|null
     * @throws SchemaNotRegisteredException
     */
    private function getCompatibleSchema(string $resourceType, array $expectedSchemas): ?ResourceSchemaInterface
    {
        foreach ($expectedSchemas as $schemaName) {
            $schema = $this->schemaManager->get($schemaName);
            if ($schema->getResourceType() == $resourceType) {
                return $schema;
            }
        }

        return null;
    }

    /**
     * Resolves a cached relationship object from the context object if it exists.
     * If not, creates a new relationship object and caches it into context cache.
     *
     * @param Context $context
     * @param ResourceSchemaInterface $schema
     * @param string $id
     * @return mixed
     */
    private function resolveRelationshipObject(Context $context, ResourceSchemaInterface $schema, string $id)
    {
        $class = $schema->getMappingClass();

        if (!$context->hasCachedObject($class, $id)) {
            $object = new $class;
            $schema->getIdentifier()->setResourceId($object, $id);
            $context->addCachedObject($class, $id, $object);
        }

        return $context->getCachedObject($class, $id);
    }
}