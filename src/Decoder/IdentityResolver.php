<?php


namespace Vallarj\JsonApi\Decoder;


use stdClass;
use Vallarj\JsonApi\Decoder\IdentityResolver\Cache;
use Vallarj\JsonApi\Exception\InvalidFormatException;
use Vallarj\JsonApi\Exception\SchemaNotRegisteredException;
use Vallarj\JsonApi\Schema\ResourceSchemaInterface;
use Vallarj\JsonApi\SchemaManager;

class IdentityResolver
{
    /** @var SchemaManager */
    private $schemaManager;

    /**
     * IdentityResolver constructor.
     *
     * @param SchemaManager $schemaManager
     */
    public function __construct(SchemaManager $schemaManager)
    {
        $this->schemaManager = $schemaManager;
    }

    /**
     * Resolves an identity object from a relationships object
     *
     * @param stdClass $data
     * @param ResourceSchemaInterface[] $schemas
     * @return mixed
     * @throws InvalidFormatException|SchemaNotRegisteredException
     */
    public function resolveToOneRelationship(stdClass $data, array $schemas)
    {
        return $this->resolveResourceIdentity($data, $schemas, new Cache());
    }

    /**
     * Resolves an array of identity objects from a collection of relationships objects
     *
     * @param stdClass[] $data
     * @param array $schemas
     * @return array
     * @throws InvalidFormatException|SchemaNotRegisteredException
     */
    public function resolveToManyRelationship(array $data, array $schemas): array
    {
        $cache = new Cache();
        $collection = [];

        foreach ($data as $item) {
            $collection[] = $this->resolveResourceIdentity($item, $schemas, $cache);
        }

        return $collection;
    }

    /**
     * Resolves an identity object from a given schema
     *
     * @param $data
     * @param ResourceSchemaInterface[] $schemas
     * @param Cache $cache
     * @return mixed
     * @throws InvalidFormatException|SchemaNotRegisteredException
     */
    public function resolveResourceIdentity($data, array $schemas, Cache $cache)
    {
        $compatibleSchema = $this->getCompatibleSchema($data->type, $schemas);

        if (!$compatibleSchema) {
            throw new InvalidFormatException("Invalid 'type' given for this resource. [$data->type]");
        }

        // Get the resource class
        $resourceClass = $compatibleSchema->getMappingClass();
        $resourceId = $data->id;

        if ($cache->hasObject($resourceClass, $resourceId)) {
            return $cache->getObject($resourceClass, $resourceClass);
        }

        // Create the object
        $object = new $resourceClass;

        // Set the resource ID
        $compatibleSchema->getIdentifier()->setResourceId($object, $resourceId);

        // Cache the object
        $cache->addObject($resourceClass, $resourceId, $object);

        // Return the object
        return $object;
    }

    /**
     * Returns a compatible schema for a given resource type
     *
     * @param string $resourceType
     * @param ResourceSchemaInterface[] $expectedSchemas
     * @return ResourceSchemaInterface|null
     * @throws SchemaNotRegisteredException
     */
    private function getCompatibleSchema(string $resourceType, array $expectedSchemas): ?ResourceSchemaInterface
    {
        foreach ($expectedSchemas as $schemaName) {
            $schema = $this->schemaManager->get($schemaName);
            if ($schema->getResourceType() == $resourceType) {
                return $schema;
            }
        }

        return null;
    }
}