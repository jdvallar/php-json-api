<?php


namespace Vallarj\JsonApi\Decoder\IdentityResolver;


class Cache
{
    /** @var array */
    private $cache = [];

    /**
     * Returns true if cached object exists
     *
     * @param string $class
     * @param string $id
     * @return bool
     */
    public function hasObject(string $class, string $id): bool
    {
        return isset($this->cache[$class][$id]);
    }

    /**
     * Returns cached object
     *
     * @param string $class
     * @param string $id
     * @return mixed
     */
    public function getObject(string $class, string $id)
    {
        return $this->cache[$class][$id] ?? null;
    }

    /**
     * Adds an object into the cache
     *
     * @param string $class
     * @param string $id
     * @param $object
     * @return void
     */
    public function addObject(string $class, string $id, $object): void
    {
        $this->cache[$class][$id] = $object;
    }
}