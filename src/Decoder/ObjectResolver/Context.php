<?php


namespace Vallarj\JsonApi\Decoder\ObjectResolver;


use ArrayAccess;
use stdClass;
use Vallarj\JsonApi\Exception\Exception;
use Vallarj\JsonApi\Schema\ResourceSchemaInterface;

class Context implements ArrayAccess
{
    private const OPERATION_POST = 'POST';
    private const OPERATION_PATCH = 'PATCH';

    /** @var string */
    private $operation;

    /** @var ResourceSchemaInterface */
    private $resourceSchema;

    /** @var mixed */
    private $identifier;

    /** @var array */
    private $attributes = [];

    /** @var stdClass[] */
    private $toOneRelationships = [];

    /** @var stdClass[][] */
    private $toManyRelationships = [];

    /** @var array */
    private $objectCache = [];

    /**
     * Context constructor.
     *
     * @param string $operation
     * @param ResourceSchemaInterface $resourceSchema
     * @throws Exception
     */
    function __construct(string $operation, ResourceSchemaInterface $resourceSchema)
    {
        if (!$this->isValidOperation($operation)) {
            throw new Exception("Invalid operation specified.");
        }

        $this->operation = $operation;
        $this->resourceSchema = $resourceSchema;
    }

    /**
     * Returns the operation
     *
     * @return string
     */
    public function getOperation(): string
    {
        return $this->operation;
    }

    /**
     * Returns the resource schema
     *
     * @return ResourceSchemaInterface
     */
    public function getResourceSchema(): ResourceSchemaInterface
    {
        return $this->resourceSchema;
    }

    /**
     * Returns the identifier
     *
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * Sets the identifier
     *
     * @param mixed $identifier
     */
    public function setIdentifier($identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * Returns true if attribute exists
     *
     * @param string $key
     * @return bool
     */
    public function hasAttribute(string $key): bool
    {
        return array_key_exists($key, $this->attributes);
    }

    /**
     * Returns an attribute
     *
     * @param string $key
     * @return mixed
     */
    public function getAttribute(string $key)
    {
        return $this->attributes[$key] ?? null;
    }

    /**
     * Returns the attributes
     *
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * Adds an attribute
     *
     * @param string $key
     * @param $value
     */
    public function addAttribute(string $key, $value): void
    {
        $this->attributes[$key] = $value;
    }

    /**
     * Returns true if to-one relationship exists
     *
     * @param string $key
     * @return bool
     */
    public function hasToOneRelationship(string $key): bool
    {
        return array_key_exists($key, $this->toOneRelationships);
    }

    /**
     * Returns to-one relationship
     *
     * @param string $key
     * @return stdClass|null
     */
    public function getToOneRelationship(string $key)
    {
        return $this->toOneRelationships[$key] ?? null;
    }

    /**
     * Returns the to-one relationships
     *
     * @return stdClass[]
     */
    public function getToOneRelationships(): array
    {
        return $this->toOneRelationships;
    }

    /**
     * Adds a to-one relationship
     *
     * @param string $key
     * @param null|stdClass $value
     */
    public function addToOneRelationship(string $key, ?stdClass $value): void
    {
        $this->toOneRelationships[$key] = $value;
    }

    /**
     * Returns true if to-many relationship exists
     *
     * @param string $key
     * @return bool
     */
    public function hasToManyRelationship(string $key): bool
    {
        return array_key_exists($key, $this->toManyRelationships);
    }

    /**
     * Returns the to-many relationships
     *
     * @return stdClass[][]
     */
    public function getToManyRelationships(): array
    {
        return $this->toManyRelationships;
    }

    /**
     * Returns to-many relationship
     *
     * @param string $key
     * @return stdClass[]
     */
    public function getToManyRelationship(string $key)
    {
        return $this->toManyRelationships[$key] ?? [];
    }

    /**
     * Adds a to-one relationship
     *
     * @param string $key
     * @param stdClass[] $collection
     */
    public function addToManyRelationship(string $key, array $collection): void
    {
        $this->toManyRelationships[$key] = $collection;
    }

    /**
     * Returns true if operation is valid
     *
     * @param string $operation
     * @return bool
     */
    private function isValidOperation(string $operation): bool
    {
        return $operation === self::OPERATION_POST || $operation === self::OPERATION_PATCH;
    }

    /**
     * Returns true if cached object exists
     *
     * @param string $class
     * @param string $id
     * @return bool
     */
    public function hasCachedObject(string $class, string $id): bool
    {
        return isset($this->objectCache[$class][$id]);
    }

    /**
     * Returns cached object
     *
     * @param string $class
     * @param string $id
     * @return mixed
     */
    public function getCachedObject(string $class, string $id)
    {
        return $this->objectCache[$class][$id] ?? null;
    }

    /**
     * Adds an object into the cache
     *
     * @param string $class
     * @param string $id
     * @param $object
     * @return void
     */
    public function addCachedObject(string $class, string $id, $object): void
    {
        $this->objectCache[$class][$id] = $object;
    }

    /**
     * @inheritDoc
     */
    public function offsetExists($offset)
    {
        return in_array($offset, ["attributes", "relationships"]);
    }

    /**
     * @inheritDoc
     */
    public function offsetGet($offset)
    {
        if ($offset === 'attributes') {
            return $this->getAttributes();
        } else if ($offset === 'relationships') {
            return array_merge($this->getToOneRelationships(), $this->getToManyRelationships());
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function offsetSet($offset, $value)
    {
        // Do nothing.
        return;
    }

    /**
     * @inheritDoc
     */
    public function offsetUnset($offset)
    {
        // Do nothing.
        return;
    }
}