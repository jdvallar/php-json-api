<?php


namespace Vallarj\JsonApi\Decoder;


use Vallarj\JsonApi\Error\ErrorDocument;

interface ResultInterface
{
    /**
     * Returns the result of the decoding operation
     *
     * @return mixed
     */
    public function getResult();

    /**
     * Returns true if validation errors were found
     *
     * @return bool
     */
    public function hasValidationErrors(): bool;

    /**
     * Returns the modified properties
     *
     * @return string[]
     */
    public function getModifiedProperties(): array;

    /**
     * Returns the error document of the operation if errors were found
     *
     * @return ErrorDocument|null
     */
    public function getErrorDocument(): ?ErrorDocument;
}