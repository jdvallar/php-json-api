<?php


namespace Vallarj\JsonApi\Decoder;


use Vallarj\JsonApi\Error\Error;
use Vallarj\JsonApi\Error\ErrorDocument;
use Vallarj\JsonApi\Error\Source\AttributePointer;
use Vallarj\JsonApi\Error\Source\RelationshipPointer;

class Result implements ResultInterface
{
    /** @var mixed */
    private $result;

    /** @var ErrorDocument|null */
    private $errorDocument;

    /** @var array */
    private $modifiedProperties;

    /**
     * Result constructor.
     *
     * @param $resultObject
     * @param array $modifiedProperties
     * @param array $attributeErrors
     * @param array $relationshipErrors
     */
    public function __construct(
        $resultObject,
        array $modifiedProperties,
        array $attributeErrors,
        array $relationshipErrors
    ) {
        $this->result = $resultObject;
        $this->modifiedProperties = $modifiedProperties;
        $this->setAttributeErrors($attributeErrors);
        $this->setRelationshipErrors($relationshipErrors);
    }

    /**
     * @inheritDoc
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @inheritDoc
     */
    public function hasValidationErrors(): bool
    {
        return !is_null($this->errorDocument);
    }

    /**
     * @inheritDoc
     */
    public function getModifiedProperties(): array
    {
        return $this->modifiedProperties;
    }

    /**
     * @inheritDoc
     */
    public function getErrorDocument(): ?ErrorDocument
    {
        return $this->errorDocument;
    }

    /**
     * Sets attribute errors
     *
     * @param array $attributeErrors
     */
    private function setAttributeErrors(array $attributeErrors)
    {
        if (!empty($attributeErrors) && is_null($this->errorDocument)) {
            $this->errorDocument = new ErrorDocument("422");
        }

        foreach ($attributeErrors as $key => $errors) {
            foreach ($errors as $detail) {
                $error = new Error();
                $error->setSource(new AttributePointer($key));
                $error->setDetail($detail);
                $this->errorDocument->addError($error);
            }
        }
    }

    /**
     * Sets relationship errors
     *
     * @param array $relationshipErrors
     */
    private function setRelationshipErrors(array $relationshipErrors)
    {
        if (!empty($relationshipErrors) && is_null($this->errorDocument)) {
            $this->errorDocument = new ErrorDocument("422");
        }

        foreach ($relationshipErrors as $key => $errors) {
            foreach ($errors as $detail) {
                $error = new Error();
                $error->setSource(new RelationshipPointer($key));
                $error->setDetail($detail);
                $this->errorDocument->addError($error);
            }
        }
    }
}
