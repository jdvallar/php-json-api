<?php


namespace Vallarj\JsonApi;


use Vallarj\JsonApi\Exception\SchemaNameTakenException;
use Vallarj\JsonApi\Exception\SchemaNotRegisteredException;
use Vallarj\JsonApi\Schema\ResourceSchemaInterface;

class SchemaManager
{
    /** @var ResourceSchemaInterface[] */
    private $schemas = [];

    /**
     * Registers a resource schema
     *
     * @param ResourceSchemaInterface $schema
     * @param string $name
     * @throws SchemaNameTakenException
     */
    public function register(ResourceSchemaInterface $schema, string $name): void
    {
        if ($this->has($name)) {
            throw new SchemaNameTakenException("Unable to register schema [$name]. Name already taken.");
        }

        $this->schemas[$name] = $schema;
    }

    /**
     * Gets a resource schema
     *
     * @param string $name
     * @return ResourceSchemaInterface
     * @throws SchemaNotRegisteredException
     */
    public function get(string $name): ResourceSchemaInterface
    {
        if(!$this->has($name)) {
            throw new SchemaNotRegisteredException("Schema [$name] not found.");
        }

        return $this->schemas[$name];
    }

    /**
     * Returns true if a registered schema with name exists
     *
     * @param string $name
     * @return bool
     */
    public function has(string $name): bool
    {
        return isset($this->schemas[$name]);
    }
}