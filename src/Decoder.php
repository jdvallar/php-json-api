<?php
/**
 *  Copyright 2017-2018 Justin Dane D. Vallar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

namespace Vallarj\JsonApi;


use stdClass;
use Vallarj\JsonApi\Decoder\IdentityResolver;
use Vallarj\JsonApi\Decoder\ObjectResolver;
use Vallarj\JsonApi\Decoder\ResultInterface;
use Vallarj\JsonApi\Exception\Exception;
use Vallarj\JsonApi\Exception\InvalidFormatException;
use Vallarj\JsonApi\Exception\InvalidIdentifierException;
use Vallarj\JsonApi\JsonSchema\JsonSchemaValidator;
use Vallarj\JsonApi\JsonSchema\JsonSchemaValidatorInterface;
use Vallarj\JsonApi\Schema\ResourceSchemaInterface;

class Decoder implements DecoderInterface
{
    private const OPERATION_POST = 'POST';
    private const OPERATION_PATCH = 'PATCH';

    /** @var SchemaManager */
    private $schemaManager;

    /** @var ObjectResolver */
    private $objectResolver;

    /** @var IdentityResolver */
    private $identityResolver;

    /** @var JsonSchemaValidatorInterface */
    private $jsonSchemaValidator;

    /**
     * Decoder constructor.
     *
     * @param SchemaManager $schemaManager
     * @param ObjectResolver $objectResolver
     * @param IdentityResolver $identityResolver
     * @param JsonSchemaValidatorInterface $validator
     */
    public function __construct(
        SchemaManager $schemaManager,
        ObjectResolver $objectResolver,
        IdentityResolver $identityResolver,
        JsonSchemaValidatorInterface $validator
    ) {
        $this->schemaManager = $schemaManager;
        $this->objectResolver = $objectResolver;
        $this->identityResolver = $identityResolver;
        $this->jsonSchemaValidator = $validator;
    }

    /**
     * Creates a Decoder instance
     *
     * @param SchemaManager $schemaManager
     * @return Decoder
     */
    public static function create(SchemaManager $schemaManager): Decoder
    {
        return new Decoder(
            $schemaManager,
            new ObjectResolver($schemaManager),
            new IdentityResolver($schemaManager),
            new JsonSchemaValidator()
        );
    }

    /**
     * @inheritdoc
     * @throws Exception
     */
    public function decodePostResource(string $data, array $schemas, bool $allowEphemeralId = false): ResultInterface
    {
        // Decode root object
        $root = json_decode($data);

        // Validate if JSON API document compliant
        if (json_last_error() !== JSON_ERROR_NONE || !$this->isValidPostDocument($root)) {
            throw new InvalidFormatException("Invalid document format.");
        }

        $data = $root->data;

        // Throw exception if ephemeral IDs are not allowed
        if(property_exists($data, 'id') && !$allowEphemeralId) {
            throw new InvalidFormatException("Ephemeral IDs are not allowed.");
        }

        return $this->decodeSingleResource($data, $schemas, self::OPERATION_POST);
    }

    /**
     * @inheritdoc
     * @throws InvalidIdentifierException
     * @throws Exception
     */
    public function decodePatchResource(string $data, array $schemas, $expectedId = null): ResultInterface
    {
        // Decode root object
        $root = json_decode($data);

        // Validate if JSON API document compliant
        if (json_last_error() !== JSON_ERROR_NONE || !$this->isValidPatchDocument($root)) {
            throw new InvalidFormatException("Invalid document format.");
        }

        $data = $root->data;

        // Check ID if expected ID was provided
        if (!is_null($expectedId) && $data->id !== $expectedId) {
            throw new InvalidIdentifierException("Resource ID must match ID provided in endpoint path.");
        }

        return $this->decodeSingleResource($data, $schemas, self::OPERATION_PATCH);
    }

    /**
     * @inheritdoc
     */
    public function decodeToOneRelationshipRequest(string $data, array $schemas)
    {
        // Decode root object
        $root = json_decode($data);

        // Validate if JSON API document compliant
        if(json_last_error() !== JSON_ERROR_NONE || !$this->isValidToOneRelationshipDocument($root)) {
            throw new InvalidFormatException("Invalid document format.");
        }

        return $this->identityResolver->resolveToOneRelationship($root->data, $schemas);
    }

    /**
     * @inheritdoc
     */
    public function decodeToManyRelationshipRequest(string $data, array $schemas)
    {
        // Decode root object
        $root = json_decode($data);

        // Validate if JSON API document compliant
        if(json_last_error() !== JSON_ERROR_NONE || !$this->isValidToManyRelationshipDocument($root)) {
            throw new InvalidFormatException("Invalid document format.");
        }

        return $this->identityResolver->resolveToManyRelationship($root->data, $schemas);
    }

    /**
     * Decode a single resource.
     *
     * @param object $data
     * @param ResourceSchemaInterface[] $schemas
     * @param string $operation
     * @return mixed
     * @throws InvalidFormatException
     * @throws Exception
     */
    private function decodeSingleResource($data, array $schemas, string $operation)
    {
        $resourceType = $data->type;
        $compatibleSchema = null;

        foreach($schemas as $schemaName) {
            $schema = $this->schemaManager->get($schemaName);
            if($schema->getResourceType() == $resourceType) {
                $compatibleSchema = $schema;
                break;
            }
        }

        if(!$compatibleSchema) {
            throw new InvalidFormatException("Invalid 'type' given for this resource");
        }

        return $this->objectResolver->resolve($data, $compatibleSchema, $operation);
    }

    /**
     * Returns true if a specified object is a valid POST document
     *
     * @param stdClass $data
     * @return bool
     */
    private function isValidPostDocument(stdClass $data): bool
    {
        return $this->jsonSchemaValidator->isValidPostDocument($data);
    }

    /**
     * Returns true if a specified object is a valid PATCH document
     *
     * @param stdClass $data
     * @return bool
     */
    private function isValidPatchDocument(stdClass $data): bool
    {
        return $this->jsonSchemaValidator->isValidPatchDocument($data);
    }

    /**
     * Returns true if a specified object is a valid to-one relationship document
     *
     * @param stdClass $data
     * @return bool
     */
    private function isValidToOneRelationshipDocument(stdClass $data): bool
    {
        return $this->jsonSchemaValidator->isValidToOneRelationshipDocument($data);
    }

    /**
     * Returns true if a specified object is a valid to-many relationship document
     *
     * @param stdClass $data
     * @return bool
     */
    private function isValidToManyRelationshipDocument(stdClass $data): bool
    {
        return $this->jsonSchemaValidator->isValidToManyRelationshipDocument($data);
    }
}