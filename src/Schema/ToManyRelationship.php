<?php
/**
 *  Copyright 2017-2018 Justin Dane D. Vallar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

namespace Vallarj\JsonApi\Schema;


use Doctrine\Inflector\Inflector;
use Doctrine\Inflector\InflectorFactory;
use Doctrine\Inflector\Rules\Pattern;
use Doctrine\Inflector\Rules\Patterns;
use Doctrine\Inflector\Rules\Ruleset;
use Doctrine\Inflector\Rules\Substitutions;
use Doctrine\Inflector\Rules\Transformation;
use Doctrine\Inflector\Rules\Transformations;
use Vallarj\JsonApi\Exception\InvalidSpecificationException;
use Vallarj\JsonApi\Exception\InvalidValidatorException;

class ToManyRelationship implements ToManyRelationshipInterface
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PATCH = 'PATCH';

    /** @var null|Inflector */
    private static $inflector = null;

    /** @var string Specifies the key of the relationship */
    private $key = "";

    /** @var string Specifies the property name of the mapped relationship */
    private $mappedAs = "";

    /** @var bool Specifies if relationship is required. */
    private $isRequired = false;

    /** @var bool Specifies if attribute is required on PATCH */
    private $isRequiredOnUpdate = false;

    /** @var bool Validate relationship if null. Default is false. */
    private $validateIfEmpty = false;

    /** @var string[] Array of expected AbstractResourceSchema FQCNs */
    private $expectedSchemas = [];

    /** @var bool Specifies if relationship is readable */
    private $isReadable = true;

    /** @var bool Specifies is attribute is writable on POST */
    private $isCreateWritable = true;

    /** @var bool Specifies is attribute is writable on PATCH */
    private $isUpdateWritable = true;

    /** @var callable   Relationship validator  */
    private $validator = null;

    /**
     * Returns the default static inflector
     *
     * @return Inflector
     */
    public function getInflector(): Inflector
    {
        if (is_null(self::$inflector)) {
            $customSingularization = new Ruleset(
                new Transformations(new Transformation(new Pattern('/^(.*)sOf(.*)$/'), '\1Of\2')),
                new Patterns(),
                new Substitutions()
            );

            $factory = InflectorFactory::create();
            $factory->withSingularRules($customSingularization);
            self::$inflector =  $factory->build();
        }

        return self::$inflector;
    }

    /**
     * Sets the default static inflector
     *
     * @param Inflector $inflector
     * @return void
     */
    public function setInflector(Inflector $inflector): void
    {
        self::$inflector = $inflector;
    }

    /**
     * @inheritdoc
     */
    public function setOptions(array $options): void
    {
        if(isset($options['key'])) {
            $this->key = $this->mappedAs = $options['key'];
        }

        if(isset($options['mappedAs'])) {
            $this->mappedAs = $options['mappedAs'];
        }

        if(isset($options['access'])) {
            if(is_array($access = $options['access'])) {
                $this->setReadable(in_array(self::GET, $access));
                $this->setCreateWritable(in_array(self::POST, $access));
                $this->setUpdateWritable(in_array(self::PATCH, $access));
            }
        }

        if(isset($options['required'])) {
            $this->setRequired($options['required']);
        }

        if(isset($options['requiredOnUpdate'])) {
            $this->setRequiredOnUpdate($options['requiredOnUpdate']);
        }

        if(isset($options['validate_if_empty'])) {
            $this->setValidateIfEmpty($options['validate_if_empty']);
        }

        if(isset($options['expects'])) {
            $expects = $options['expects'];

            if(!is_array($expects)) {
                throw new InvalidSpecificationException("Index 'expects' must be a compatible array");
            }

            $this->expectedSchemas = $expects;
        }

        if(isset($options['validator'])) {
            $validator = $options['validator'];

            if(!is_callable($validator)) {
                throw new InvalidSpecificationException("Index 'validator' must be a compatible callable.");
            }

            $this->setValidator($validator);
        }
    }

    /**
     * @inheritdoc
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @inheritdoc
     */
    public function getExpectedSchemas(): array
    {
        return $this->expectedSchemas;
    }

    /**
     * @inheritdoc
     */
    public function getCollection($parentObject): array
    {
        return $parentObject->{'get' . ucfirst($this->mappedAs)}();
    }

    /**
     * @inheritdoc
     */
    public function addItem($parentObject, $object): void
    {
        $inflector = $this->getInflector();
        $singularMapping = $inflector->singularize($this->mappedAs);
        $parentObject->{'add' . ucfirst($singularMapping)}($object);
    }

    /**
     * @inheritdoc
     */
    public function clearCollection($parentObject): void
    {
        $collection = $this->getCollection($parentObject);

        // Assumes there is a remove function
        foreach($collection as $item) {
            $parentObject->{'remove' . ucfirst($this->mappedAs)}($item);
        }
    }

    /**
     * @inheritdoc
     */
    public function isReadable(): bool
    {
        return $this->isReadable;
    }

    /**
     * @inheritDoc
     */
    public function isCreateWritable(): bool
    {
        return $this->isCreateWritable;
    }

    /**
     * @inheritDoc
     */
    public function isUpdateWritable(): bool
    {
        return $this->isUpdateWritable;
    }

    /**
     * Sets the isReadable flag of this relationship
     * @param bool $isReadable
     */
    private function setReadable(bool $isReadable)
    {
        $this->isReadable = $isReadable;
    }

    /**
     * Sets the create writable flag of this attribute
     * @param bool $isCreateWritable
     */
    private function setCreateWritable(bool $isCreateWritable)
    {
        $this->isCreateWritable = $isCreateWritable;
    }

    /**
     * Sets the update writable flag of this attribute
     * @param bool $isUpdateWritable
     */
    private function setUpdateWritable(bool $isUpdateWritable)
    {
        $this->isUpdateWritable = $isUpdateWritable;
    }

    /**
     * @inheritdoc
     */
    public function isValid(array $relationships, $context): ValidationResultInterface
    {
        if(is_callable($this->validator)) {
            $validator = $this->validator;
            $result = $validator($relationships, $context);

            if(!$result instanceof ValidationResultInterface) {
                throw new InvalidValidatorException("Relationship validator must return an instance of ValidationResultInterface");
            }

            return $result;
        }

        return new ValidationResult(true);
    }

    /**
     * Callable that should return ValidationResultInterface
     * @param callable $validator
     */
    public function setValidator(callable $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @inheritdoc
     */
    public function validateIfEmpty(): bool
    {
        return $this->validateIfEmpty;
    }

    /**
     * Sets the validation if empty flag
     * @param bool $validateIfEmpty
     */
    public function setValidateIfEmpty(bool $validateIfEmpty)
    {
        $this->validateIfEmpty = $validateIfEmpty;
    }

    /**
     * @inheritdoc
     */
    public function isRequired(): bool
    {
        return $this->isRequired;
    }

    /**
     * @inheritdoc
     */
    public function isRequiredOnUpdate(): bool
    {
        return $this->isRequiredOnUpdate;
    }

    /**
     * Sets the relationship required flag
     * @param bool $required
     */
    public function setRequired(bool $required)
    {
        $this->isRequired = $required;
    }

    /**
     * Sets the attribute required flag
     * @param bool $requiredOnUpdate
     */
    public function setRequiredOnUpdate(bool $requiredOnUpdate)
    {
        $this->isRequiredOnUpdate = $requiredOnUpdate;
    }
}