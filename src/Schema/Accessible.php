<?php


namespace Vallarj\JsonApi\Schema;


interface Accessible
{
    /**
     * Returns true if property is readable
     * @return bool
     */
    public function isReadable(): bool;

    /**
     * Returns true if property is writable on POST
     * @return bool
     */
    public function isCreateWritable(): bool;

    /**
     * Returns true if property is writable on PATCH
     * @return bool
     */
    public function isUpdateWritable(): bool;
}