<?php


namespace Vallarj\JsonApi\Schema;


interface Requirable
{
    /**
     * Returns true if attribute is required on POST
     * @return bool
     */
    public function isRequired(): bool;

    /**
     * Returns true if attribute is required on PATCH
     * @return bool
     */
    public function isRequiredOnUpdate(): bool;
}