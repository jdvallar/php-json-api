<?php
/**
 *  Copyright 2017-2018 Justin Dane D. Vallar
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 */

namespace Vallarj\JsonApi\Schema;


use Laminas\Validator;

class DateAttribute implements AttributeInterface
{
    public const GET = 'GET';
    public const POST = 'POST';
    public const PATCH = 'PATCH';

    /** @var string Specifies the key of the attribute */
    private $key = "";

    /** @var bool Specifies if attribute is required. */
    private $isRequired = false;

    /** @var bool Specifies if attribute is required on PATCH */
    private $isRequiredOnUpdate = false;

    /** @var bool Specifies if attribute is readable. */
    private $isReadable = true;

    /** @var bool Specifies is attribute is writable on POST */
    private $isCreateWritable = true;

    /** @var bool Specifies is attribute is writable on PATCH */
    private $isUpdateWritable = true;

    /** @var Validator\Date Date validator */
    private $validator;

    /**
     * @inheritdoc
     */
    public function setOptions(array $options): void
    {
        if(isset($options['key'])) {
            $this->key = $options['key'];
        }

        if(isset($options['required'])) {
            $this->setRequired($options['required']);
        }

        if(isset($options['requiredOnUpdate'])) {
            $this->setRequiredOnUpdate($options['requiredOnUpdate']);
        }

        if(isset($options['access'])) {
            if(is_array($access = $options['access'])) {
                $this->setReadable(in_array(self::GET, $access));
                $this->setCreateWritable(in_array(self::POST, $access));
                $this->setUpdateWritable(in_array(self::PATCH, $access));
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @inheritdoc
     */
    public function getValue($parentObject)
    {
        $dateTimeValue = $parentObject->{'get' . ucfirst($this->key)}();
        if($dateTimeValue instanceof \DateTime) {
            return $dateTimeValue->format(\DateTime::ATOM);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function setValue($parentObject, $value): void
    {
        if(!is_null($value)) {
            $value = \DateTime::createFromFormat(DATE_ATOM, $value);
        }
        $parentObject->{'set' . ucfirst($this->key)}($value);
    }

    /**
     * @inheritdoc
     */
    public function isReadable(): bool
    {
        return $this->isReadable;
    }

    /**
     * @inheritDoc
     */
    public function isCreateWritable(): bool
    {
        return $this->isCreateWritable;
    }

    /**
     * @inheritDoc
     */
    public function isUpdateWritable(): bool
    {
        return $this->isUpdateWritable;
    }

    /**
     * Sets the readable flag of this attribute
     * @param bool $isReadable
     */
    private function setReadable(bool $isReadable)
    {
        $this->isReadable = $isReadable;
    }

    /**
     * Sets the create writable flag of this attribute
     * @param bool $isCreateWritable
     */
    private function setCreateWritable(bool $isCreateWritable)
    {
        $this->isCreateWritable = $isCreateWritable;
    }

    /**
     * Sets the update writable flag of this attribute
     * @param bool $isUpdateWritable
     */
    private function setUpdateWritable(bool $isUpdateWritable)
    {
        $this->isUpdateWritable = $isUpdateWritable;
    }

    /**
     * Gets the validator of this attribute
     * @return Validator\Date
     */
    private function getValidator(): Validator\Date
    {
        if(!$this->validator) {
            $this->validator = new Validator\Date(["format" => \DateTime::ATOM]);
            $this->validator->setMessage("Date must follow ISO-8601 format", Validator\Date::INVALID_DATE);
            $this->validator->setMessage("Date must follow ISO-8601 format", Validator\Date::FALSEFORMAT);
        }

        return $this->validator;
    }

    /**
     * @inheritdoc
     */
    public function filterValue($value)
    {
        // Do not perform any pre-processing
        return $value;
    }

    /**
     * @inheritdoc
     */
    public function isValid($value, $context): ValidationResultInterface
    {
        // Workaround for null $value
        if(is_null($value)) {
            $value = "";
        }

        $validator = $this->getValidator();
        $validationResult = new ValidationResult($validator->isValid($value));
        $messages = $validator->getMessages();
        foreach($messages as $message) {
            $validationResult->addMessage($message);
        }

        return $validationResult;
    }

    /**
     * @inheritdoc
     */
    public function validateIfEmpty(): bool
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function isRequired(): bool
    {
        return $this->isRequired;
    }

    /**
     * Sets the attribute required flag
     * @param bool $required
     */
    public function setRequired(bool $required)
    {
        $this->isRequired = $required;
    }

    /**
     * @inheritdoc
     */
    public function isRequiredOnUpdate(): bool
    {
        return $this->isRequiredOnUpdate;
    }

    /**
     * Sets the attribute required flag
     * @param bool $requiredOnUpdate
     */
    public function setRequiredOnUpdate(bool $requiredOnUpdate)
    {
        $this->isRequiredOnUpdate = $requiredOnUpdate;
    }
}