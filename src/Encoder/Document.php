<?php


namespace Vallarj\JsonApi\Encoder;


use JsonSerializable;

interface Document extends JsonSerializable
{
    /**
     * Puts the data into the document
     *
     * @param string $type
     * @param $id
     * @param array $attributes
     * @param array $relationships
     * @param array $meta
     * @return void
     */
    public function putData(string $type, $id, array $attributes, array $relationships, array $meta): void;

    /**
     * Returns true if document has resource, either in data or included
     *
     * @param string $resourceType
     * @param $id
     * @return bool
     */
    public function hasResource(string $resourceType, $id): bool;

    /**
     * Returns true if document has a data resource
     *
     * @param string $resourceType
     * @param $id
     * @return bool
     */
    public function hasData(string $resourceType, $id): bool;

    /**
     * Returns true if document has an included resource
     *
     * @param string $resourceType
     * @param $id
     * @return bool
     */
    public function hasIncluded(string $resourceType, $id): bool;

    /**
     * Adds an item as an included resource
     *
     * @param string $type
     * @param $id
     * @param array $attributes
     * @param array $relationships
     * @param array $meta
     * @return void
     */
    public function addIncluded(string $type, $id, array $attributes, array $relationships, array $meta): void;

    /**
     * Removes an item from the included resources
     * Returns the resource object if it exists
     *
     * @param string $type
     * @param $id
     * @return array
     */
    public function removeIncluded(string $type, $id): ?array;

    /**
     * Sets the document root meta
     *
     * @param array $meta
     */
    public function setMeta(array $meta): void;
}