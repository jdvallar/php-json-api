<?php


namespace Vallarj\JsonApi\Encoder;


use Vallarj\JsonApi\Encoder\Document\CollectionDocument;
use Vallarj\JsonApi\Encoder\ResourceExtractor\Context;
use Vallarj\JsonApi\Encoder\Document\ResourceDocument;
use Vallarj\JsonApi\Exception\Exception;
use Vallarj\JsonApi\Exception\SchemaNotRegisteredException;
use Vallarj\JsonApi\Schema\ResourceSchemaInterface;
use Vallarj\JsonApi\Schema\ToManyRelationshipInterface;
use Vallarj\JsonApi\Schema\ToOneRelationshipInterface;
use Vallarj\JsonApi\SchemaManager;

class ResourceExtractor
{
    /** @var SchemaManager */
    private $schemaManager;

    /**
     * ResourceExtractor constructor.
     *
     * @param SchemaManager $schemaManager
     */
    public function __construct(SchemaManager $schemaManager)
    {
        $this->schemaManager = $schemaManager;
    }

    /**
     * Extracts a single resource
     *
     * @param $resource
     * @param ResourceSchemaInterface[] $schemas
     * @param array $includedKeys
     * @param callable $showProperty
     * @return Document
     * @throws Exception
     */
    public function extractSingleResource(
        $resource,
        array $schemas,
        array $includedKeys,
        callable $showProperty
    ): Document {
        $context = new Context($includedKeys);
        $document = new ResourceDocument();

        if (is_null($resource)) {
            return $document;
        }

        $this->extractResource($context, $document, $resource, $schemas, $showProperty);

        return $document;
    }

    /**
     * Extracts a resource collection
     *
     * @param array $collection
     * @param ResourceSchemaInterface[] $schemas
     * @param array $includedKeys
     * @param callable $showProperty
     * @return Document
     * @throws Exception
     */
    public function extractResourceCollection(
        array $collection,
        array $schemas,
        array $includedKeys,
        callable $showProperty
    ): Document {
        $context = new Context($includedKeys);
        $document = new CollectionDocument();

        foreach ($collection as $resource) {
            $this->extractResource($context, $document, $resource, $schemas, $showProperty);
        }

        return $document;
    }

    /**
     * Extracts an object and updates the context and document objects
     *
     * @param Context $context
     * @param Document $document
     * @param $object
     * @param array $schemas
     * @param callable $showProperty
     * @return array
     * @throws Exception
     */
    public function extractResource(
        Context $context,
        Document $document,
        $object,
        array $schemas,
        callable $showProperty
    ): array {
        $resourceClass = get_class($object);

        $schema = $this->getCompatibleSchema($resourceClass, $schemas);
        if (!$schema) {
            throw new Exception("No compatible schema found for the given resource object. [$resourceClass]");
        }

        // Get resource type and ID
        $resourceType = $schema->getResourceType();
        $id = $schema->getIdentifier()->getResourceId($object);

        if (!$context->currentStepIncluded()) {
            return [
                'type' => $resourceType,
                'id' => $id
            ];
        }

        // Extract relationships
        $relationships = $this->extractRelationships($context, $document, $object, $schema, $showProperty);

        if (
            $context->currentStep() === ""
            && !$document->hasData($resourceType, $id)
            && $document->hasIncluded($resourceType, $id)
        ) {
            // Remove from included
            $resource = $document->removeIncluded($resourceType, $id);
            $document->putData(
                $resourceType,
                $id,
                $resource['attributes'] ?? [],
                $resource['relationships'] ?? [],
                $resource['meta'] ?? []
            );
        } else if (!$document->hasResource($resourceType, $id)) {
            // If it does not exist in document yet
            // Extract attributes
            $attributes = $this->extractAttributes($object, $schema, $showProperty);

            // Extract meta
            $meta = $this->extractMeta($object, $schema, $showProperty);

            if ($context->currentStep() === "") {
                // Context at document root
                $document->putData($resourceType, $id, $attributes, $relationships, $meta);
            } else {
                // Context at relationships
                $document->addIncluded($resourceType, $id, $attributes, $relationships, $meta);
            }
        }

        return [
            'type' => $resourceType,
            'id' => $id
        ];
    }

    /**
     * Extract the attributes of an object
     *
     * @param $object
     * @param ResourceSchemaInterface $schema
     * @param callable $showProperty
     * @return array
     */
    private function extractAttributes($object, ResourceSchemaInterface $schema, callable $showProperty): array
    {
        $attributes = [];
        $schemaAttributes = $schema->getAttributes();
        $resourceType = $schema->getResourceType();
        foreach ($schemaAttributes as $schemaAttribute) {
            $key = $schemaAttribute->getKey();
            if ($showProperty($resourceType, $key) && $schemaAttribute->isReadable()) {
                $attributes[$key] = $schemaAttribute->getValue($object);
            }
        }

        return $attributes;
    }

    /**
     * Extract the relationships of an object
     *
     * @param Context $context
     * @param Document $document
     * @param $object
     * @param ResourceSchemaInterface $schema
     * @param callable $showProperty
     * @return array
     * @throws Exception
     */
    private function extractRelationships(
        Context $context,
        Document $document,
        $object,
        ResourceSchemaInterface $schema,
        callable $showProperty
    ): array {
        $relationships = [];
        $schemaRelationships = $schema->getRelationships();
        $resourceType = $schema->getResourceType();

        /** @var ToOneRelationshipInterface|ToManyRelationshipInterface $schemaRelationship */
        foreach ($schemaRelationships as $schemaRelationship) {
            $key = $schemaRelationship->getKey();

            if (!$showProperty($resourceType, $key) || !$schemaRelationship->isReadable()) {
                // Skip non-readable attributes
                continue;
            }

            $expectedSchemas = $schemaRelationship->getExpectedSchemas();

            // Step included walker
            $context->stepForward($key);

            if ($schemaRelationship instanceof ToOneRelationshipInterface) {
                $mappedObject = $schemaRelationship->getObject($object);

                if (!is_null($mappedObject)) {
                    $relationships[$key]['data'] = $this->extractResource(
                        $context, $document, $mappedObject, $expectedSchemas, $showProperty
                    );
                }
            } else if($schemaRelationship instanceof ToManyRelationshipInterface) {
                $collection = $schemaRelationship->getCollection($object);

                if (!empty($collection)) {
                    foreach ($collection as $mappedObject) {
                        $relationships[$key]['data'][] = $this->extractResource(
                            $context, $document, $mappedObject, $expectedSchemas, $showProperty
                        );
                    }
                }
            }

            // Step backward
            $context->stepBackward();
        }

        return $relationships;
    }

    /**
     * Extract meta information from an object
     *
     * @param $object
     * @param ResourceSchemaInterface $schema
     * @param callable $showProperty
     * @return array
     */
    private function extractMeta($object, ResourceSchemaInterface $schema, callable $showProperty): array
    {
        $meta = [];
        $schemaMeta = $schema->getMeta();
        $resourceType = $schema->getResourceType();
        foreach ($schemaMeta as $metaItem) {
            $key = $metaItem->getKey();
            if ($showProperty($resourceType, $key)) {
                $meta[$key] = $metaItem->getValue($object);
            }
        }

        return $meta;
    }

    /**
     * Returns a compatible schema for a given resource type
     *
     * @param string $mappingClass
     * @param string[] $expectedSchemas
     * @return ResourceSchemaInterface|null
     * @throws SchemaNotRegisteredException
     */
    private function getCompatibleSchema(string $mappingClass, array $expectedSchemas): ?ResourceSchemaInterface
    {
        foreach ($expectedSchemas as $schemaName) {
            $schema = $this->schemaManager->get($schemaName);
            if ($schema->getMappingClass() == $mappingClass) {
                return $schema;
            }
        }

        return null;
    }
}
