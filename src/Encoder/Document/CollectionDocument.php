<?php


namespace Vallarj\JsonApi\Encoder\Document;


class CollectionDocument extends AbstractDocument
{
    /** @var array Holds the data */
    private $data = [];

    /** @var array Holds the included resources */
    private $included = [];

    /** @var array Holds the root meta */
    private $meta = [];

    /**
     * Adds a data object
     *
     * @param string $type
     * @param $id
     * @param array $attributes
     * @param array $relationships
     * @param array $meta
     * @return void
     */
    public function addData(string $type, $id, array $attributes, array $relationships, array $meta): void
    {
        $this->data[$type][$id] = $this->createResourceObject($type, $id, $attributes, $relationships, $meta);
    }

    /**
     * @inheritDoc
     */
    public function putData(string $type, $id, array $attributes, array $relationships, array $meta): void
    {
        $this->addData($type, $id, $attributes, $relationships, $meta);
    }

    /**
     * @inheritDoc
     */
    public function hasResource(string $resourceType, $id): bool
    {
        return $this->hasData($resourceType, $id) || $this->hasIncluded($resourceType, $id);
    }

    /**
     * @inheritDoc
     */
    public function hasData(string $resourceType, $id): bool
    {
        return isset($this->data[$resourceType][$id]);
    }

    /**
     * @inheritDoc
     */
    public function hasIncluded(string $resourceType, $id): bool
    {
        return isset($this->included[$resourceType][$id]);
    }

    /**
     * @inheritDoc
     */
    public function addIncluded(string $type, $id, array $attributes, array $relationships, array $meta): void
    {
        $this->included[$type][$id] = $this->createResourceObject($type, $id, $attributes, $relationships, $meta);
    }

    /**
     * @inheritDoc
     */
    public function removeIncluded(string $type, $id): ?array
    {
        if ($this->hasIncluded($type, $id)) {
            $included = $this->included[$type][$id];
            unset($this->included[$type][$id]);
            return $included;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public function setMeta(array $meta): void
    {
        $this->meta = $meta;
    }

    /**
     * @inheritDoc
     */
    public function jsonSerialize()
    {
        $root = [
            'data' => $this->flattenArray($this->data)
        ];

        if (!empty($this->included)) {
            $root['included'] = $this->flattenArray($this->included);
        }

        if (!empty($meta)) {
            $root['meta'] = $this->meta;
        }

        return $root;
    }

    /**
     * Flatten array
     *
     * @param array $array
     * @return array
     */
    private function flattenArray(array $array): array
    {
        $newArray = [];

        foreach ($array as $byType) {
            foreach ($byType as $byId) {
                $newArray[] = $byId;
            }
        }

        return $newArray;
    }
}