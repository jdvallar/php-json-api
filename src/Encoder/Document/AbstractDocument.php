<?php


namespace Vallarj\JsonApi\Encoder\Document;


use Vallarj\JsonApi\Encoder\Document;

abstract class AbstractDocument implements Document
{
    /**
     * Creates a resource object
     *
     * @param string $type
     * @param $id
     * @param array $attributes
     * @param array $relationships
     * @param array $meta
     * @return array
     */
    protected function createResourceObject(
        string $type,
        $id,
        array $attributes,
        array $relationships,
        array $meta
    ): array {
        $resource = [
            'type' => $type,
            'id' => $id
        ];

        if (!empty($attributes)) {
            $resource['attributes'] = $attributes;
        }

        if (!empty($relationships)) {
            $resource['relationships'] = $relationships;
        }

        if (!empty($meta)) {
            $resource['meta'] = $meta;
        }

        return $resource;
    }
}