<?php


namespace Vallarj\JsonApi\Encoder\ResourceExtractor;


class Context
{
    /** @var array Keys of relationships to include in the current operation */
    private $includedKeys;

    /** @var array Holds the keys of the current relationship being extracted and its parents */
    private $includedWalker = [];

    /**
     * Context constructor.
     *
     * @param array $includedKeys
     */
    public function __construct(array $includedKeys)
    {
        $this->includedKeys = array_flip($includedKeys);
    }

    /**
     * Returns the current context step
     *
     * @return string
     */
    public function currentStep(): string
    {
        return implode('.', $this->includedWalker);
    }

    /**
     * Returns true if current step is included
     *
     * @return bool
     */
    public function currentStepIncluded(): bool
    {
        return empty($this->includedWalker) || isset($this->includedKeys[$this->currentStep()]);
    }

    /**
     * Pushes a key into the walker
     *
     * @param string $key
     */
    public function stepForward(string $key): void
    {
        array_push($this->includedWalker, $key);
    }

    /**
     * Pops the latest key from the walker
     */
    public function stepBackward(): ?string
    {
        if (!empty($this->includedWalker)) {
            return array_pop($this->includedWalker);
        }

        return null;
    }
}